# Made by tuzu

########################
### GENERAL SETTINGS ###
########################

# Lines configured by zsh-newuser-install
HISTFILE=~/.config/zsh/zsh_history
HISTSIZE=500
SAVEHIST=500
bindkey -v
# End of lines configured by zsh-newuser-install

# A better Prompt
precmd() { print "" }
PS1='%F{magenta}%~%f %F{blue}->%f'
PS2='%F{blue}>_%f'
RPROMPT='%F{blue}%* %W %f'

# auto complete
autoload -U compinit
zstyle ':completion:*' menu select
zmodload zsh/complist
compinit

############
### LFCD ###
############
bindkey -s '^f' 'lfcd\n'

lfcd () {
    tmp="$(mktemp)"
    lf -last-dir-path="$tmp" "$@"
    if [ -f "$tmp" ]; then
        dir="$(cat "$tmp")"
        rm -f "$tmp"
        if [ -d "$dir" ]; then
            if [ "$dir" != "$(pwd)" ]; then
                cd "$dir"
            fi
        fi
    fi
}

###############
### ALIASIS ###
###############

# vim
alias v='nvim'
alias :wq='exit'
alias :q='exit'

# Changing "ls" to "exa"
alias ls='exa --icons -la --group-directories-first --color=always' # my preferred listing
alias lt='exa -Ta --color=always --group-directories-first --icons' # tree listing

# rickroll
alias rr='curl -s -L https://raw.githubusercontent.com/keroserene/rickrollrc/master/roll.sh | bash'

# cd
alias ..='cd ..'
alias ...='cd ../..'
alias ....='cd ../../..'

# confirm before deleting something
alias cp='cp -i'
alias mv='mv -i'
alias rm='rm -i'

# tty-clock
alias clock='tty-clock -csC6'

# xbps stuf
alias xi="doas xbps-install"
alias xr="doas xbps-remove -R"
alias xq="xbps-query"

# changing shell
alias tobash="sudo chsh $USER -s /bin/bash"
alias tozsh="sudo chsh $USER -s /bin/zsh"

# lf
alias lf='lfub'

#colors are great
alias clear='clear && pfetch'
alias diff='diff --color=auto'

####################
### AUTOSTARTING ###
####################

# neofetch
# fm6000 -nd --de=i3 -c cyan
pfetch
# pokemon-colorscripts.sh -r
#eval "$(starship init zsh)" # Starship prompt

#autosuggestions.
source /usr/share/zsh/plugins/zsh-autosuggestions/zsh-autosuggestions.plugin.zsh 2>/dev/null

# syntax highlighting
#source /usr/share/zsh/plugins/zsh-syntax-highlighting/zsh-syntax-highlighting.plugin.zsh 2>/dev/null
source /home/tuzu/.config/zsh/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh
