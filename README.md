# Just my dotfiles

This repo contain just my dotfiles. 

## stuf that i actually use

- For text editor i use nvim.
- My current window manager is bspwm but I also like i3 dwm and openbox.
- For a bar I use polybar for i3 and bspwm tint2 for openbox and dwm bar with slstatus for dwm but curently a dont use bar.
- My web browser is librewolf.
- My terminal emulator is alacritty but I experiment with st.
